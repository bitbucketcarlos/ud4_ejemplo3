package com.example.ud4_ejemplo3

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.collections.ArrayList

class VehiculoAdapter (private val lista: ArrayList<Vehiculo>): RecyclerView.Adapter<VehiculoAdapter.MiViewHolder>() {

    private var listener: View.OnClickListener? = null

    // Creamos nuestro propio ViewHolder
    class MiViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imagen: ImageView
        val texto: TextView

        init {
            imagen = view.findViewById(R.id.imagenVehiculo)
            texto = view.findViewById(R.id.textoVehiculo)
        }
    }

    // Creamos nuevas views inflando el layout "elementos_lista"
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): MiViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.elementos_tarjeta, viewGroup, false)

        view.setOnClickListener(listener)

        return MiViewHolder(view)
    }

    // Establecemos el nombre y la aparición para el vehículo de esa posición
    override fun onBindViewHolder(viewHolder: MiViewHolder, position: Int) {
        val vehiculo = lista[position]

        val uriImagen = Uri.parse("android.resource://com.example.ud4_ejemplo3/drawable/" + vehiculo.imagen)

        viewHolder.imagen.setImageURI(uriImagen)
        viewHolder.texto.text = lista[position].nombre
    }

    // Devolvemos el tamaño de la lista de Vehículos
    override fun getItemCount() = lista.size

    fun setOnItemClickListerner(onClickListener: View.OnClickListener){
        listener = onClickListener
    }
}