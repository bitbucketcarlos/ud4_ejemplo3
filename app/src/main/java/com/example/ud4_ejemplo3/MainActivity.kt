package com.example.ud4_ejemplo3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.ud4_ejemplo3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Creamos el array de vehiculos.
        val vehiculos = ArrayList<Vehiculo>()

        vehiculos.add(Vehiculo("Ecto1", "Los cazafantasmas", "ecto1"))
        vehiculos.add(Vehiculo("DeLorean", "Regreso al futuro", "delorian"))
        vehiculos.add(Vehiculo("Kitt", "El coche fantástico", "kitt"))
        vehiculos.add(Vehiculo("Halcón Milenario", "Star Wars", "halcon"))
        vehiculos.add(Vehiculo("TARDIS", "Doctor Who", "tardis"))

        // Buscamos el RecyclerView e indicamos que su tamaño es fijo
        val recycler = binding.recyclerView

        recycler.setHasFixedSize(true)

        // Asignamos un GridLayout de 2 columnas
        recycler.layoutManager = GridLayoutManager(this, 2)

        val adapter = VehiculoAdapter(vehiculos)

        // Asignamos el evento creado en VehiculoAdapter
        adapter.setOnItemClickListerner() {
            val vehiculo = vehiculos.get(recycler.getChildAdapterPosition(it))
            Toast.makeText(this, vehiculo.aparicion, Toast.LENGTH_LONG).show()
        }

        // Asignamos el adapter al RecyclerView
        recycler.adapter = adapter
    }
}